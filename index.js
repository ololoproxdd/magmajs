//Алгоритм блочного шифрования с длиной блока n = 64 бит
magmaApp = angular.module('magmaApp');

magmaApp.controller('magmaController', function ($scope) {

    $scope.result_enc = 'Результат шифрования';
    $scope.encrypt = function () {
        var value = $scope.value_enc; //'fedcba9876543210';
        var key = $scope.key_enc; //'ffeeddccbbaa99887766554433221100f0f1f2f3f4f5f6f7f8f9fafbfcfdfeff';
        if (value.length == 0 || key.length == 0)
            return;
        if (key.length < 64) {
            alert('Длина ключа шифрования должна быть 64 символа!')
            return;
        }
        if (value.length < 16) {
            alert('Длина шифруемого числа должна быть 16 символов!')
            return;
        }
        var keys = keyDeployment(key);
        var object = {
            a1: value.substr(0, value.length / 2),
            a0: value.substr(value.length / 2, value.length / 2)
        };
        var i = 0;
        for (i = 0; i < 31; i++) {
            object = G(keys[i], object.a1, object.a0);
        }
        $scope.result_enc = GG(keys[i], object.a1, object.a0);
    };

    $scope.result_dec = 'Результат дешифрования';
    $scope.decrypt = function () {
        var value = $scope.value_dec; //'4ee901e5c2d8ca3d';
        var key = $scope.key_dec; //'ffeeddccbbaa99887766554433221100f0f1f2f3f4f5f6f7f8f9fafbfcfdfeff';
        if (value.length == 0 || key.length == 0)
            return;
        if (key.length < 64) {
            alert('Длина ключа шифрования должна быть 64 символа!')
            return;
        }
        if (value.length < 16) {
            alert('Длина шифруемого числа должна быть 16 символов!')
            return;
        }
        var keys = keyDeployment(key);
        var object = {
            a1: value.substr(0, value.length / 2),
            a0: value.substr(value.length / 2, value.length / 2)
        };
        var i = 0;
        for (i = 31; i > 0; i--) {
            object = G(keys[i], object.a1, object.a0);
        }
        $scope.result_dec = GG(keys[i], object.a1, object.a0);
    }

    $scope.encryptImage = function () {
        //Генерируем ключ
        var key = GenerateKey();
        console.log(key);
        var canvas = document.getElementById('heart');
        if (canvas.getContext) {
            var ctx = canvas.getContext('2d');
            var img = new Image(); // Create new img element
            img.src = 'heart.bmp';
            img.addEventListener("load", function () {
                ctx.drawImage(img, 0, 0);
                var imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
                console.log(imageData);
                console.log(imageData.data);
                var black = 0;
                for (var i = 0; i < imageData.data.length; i++)
                    if (imageData.data[i] == 0)
                        black++;
                console.log(black);
            }, false);
           
        }

    };
});


//#region Вспомогательные методы

var GenerateKey = function () {
    var array = new Uint32Array(8);
    window.crypto.getRandomValues(array);
    var key = '';
    for (var i = 0; i < array.length; i++) {
        key += FromRadix1ToRadix2(array[i], 10, 16);
    }
    while (key.length < 64)
        key = '0' + key;
    return key;
};

//Заполнение нулями старших битов
var FillingZero = function (value) {
    value = String(value);
    while (value.length < 32) {
        value = '0' + value;
    }
    return value;
};

var NumberToLetter = function (value) {
    var result = String(value);
    if (value > 9) {
        value = parseInt(value);
        switch (value) {
            case 10:
                result = 'A';
                break;
            case 11:
                result = 'B';
                break;
            case 12:
                result = 'C';
                break;
            case 13:
                result = 'D';
                break;
            case 14:
                result = 'E';
                break;
            case 15:
                result = 'F';
                break;
        }
    }
    return result;
};

var AdditionInRing = function (value1, value2) {
    return (parseInt(value1) + parseInt(value2) % 4294967296);
}

var AdditionByModule = function (value1, value2, module) {
    value1 = FillingZero(value1);
    value2 = FillingZero(value2);
    var result = "";
    for (var i = 31; i > -1; i--) {
        if (value1[i] == value2[i])
            result = '0' + result;
        else
            result = '1' + result;
    }
    return result;
};
//value - двоичное число
var CycleShift = function (value) {
    value = FillingZero(value);
    return value.substr(11, 21) + value.substr(0, 11);
}

var FromRadixToDec = function (value, radix) {
    return parseInt(String(value), radix);
};

var FromDecToRadix = function (value, radix) {
    var letter = []
    var value = parseInt(value);
    result = '';
    while (value > 0) {
        result = NumberToLetter(value % radix) + result;
        value = parseInt(value / radix);
    }
    return result;
};

var FromRadix1ToRadix2 = function (value, radix1, radix2) {
    return FromDecToRadix(FromRadixToDec(value, radix1), radix2);
};

//Из 10 в 2 (работает!)
var Vector = function (value) {
    return FromDecToRadix(value, 2);
};

//из 2 в 10 (работает!)
var Int = function (value) {
    return FromRadixToDec(value, 2);
};

//16 в 10 (работате!)
var HexToDec = function (value) {
    return FromRadixToDec(value, 16);
};

//Нелинейное биективное преобразование (работает!)
// Pi = t
var Pi = function (value) {
    var PiArray = [
        ["C", "4", "6", "2", "A", "5", "B", "9", "E", "8", "D", "7", "0", "3", "F", "1"],
        ["6", "8", "2", "3", "9", "A", "5", "C", "1", "E", "4", "7", "B", "D", "0", "F"],
        ["B", "3", "5", "8", "2", "F", "A", "D", "E", "1", "7", "4", "C", "9", "6", "0"],
        ["C", "8", "2", "1", "D", "4", "F", "6", "7", "0", "A", "5", "3", "E", "9", "B"],
        ["7", "F", "5", "A", "8", "1", "6", "D", "0", "9", "3", "E", "B", "4", "2", "C"],
        ["5", "D", "F", "6", "9", "2", "C", "A", "B", "7", "8", "1", "4", "3", "E", "0"],
        ["8", "E", "2", "5", "6", "9", "1", "C", "F", "4", "B", "0", "D", "A", "3", "7"],
        ["1", "7", "E", "D", "0", "5", "8", "3", "4", "F", "A", "6", "9", "C", "B", "2"]
    ];
    var i = 0;
    var result = "";
    var radix = 16;
    while (value.length < 8)
        value = '0' + value;
    value = parseInt(value, radix);
    while (i < 8) {
        result = PiArray[i][parseInt(value % radix)] + result;
        value = parseInt(value / radix);
        i++;
    }
    return result;
};

// Преобразования
// t = Pi
// t(fdb97531) = 2a196f34
var t = function (value) {
    return Pi(HexToDec(value));
};

// g
// g[87654321](fedcba98) = fdcbc20c
var g = function (k, a) {
    //var res1 = HexToDec("7E791A4B");
    //var res2 = HexToDec("fdcbc20c");
    var res1 = HexToDec(k);
    var res2 = HexToDec(a);
    var res3 = (res1 + res2) % 4294967296;
    var res4 = Vector(res3);
    var res5 = Pi(FromRadix1ToRadix2(res4, 2, 16));
    var res6 = Vector(HexToDec(res5));
    var res7 = CycleShift(res6);
    var res8 = FromRadix1ToRadix2(res7, 2, 16);
    return res8;
};

// G
// (a1, a0) = (fedcba98, 76543210),
// G[K1](a1, a0) = (76543210, 28da3b14)
// G[k](a1, a0) = (a0, g[k](a0) ⊕ a1)
var G = function (key, a1, a0) {
    var res0 = g(key, a0);
    var res1 = FromRadix1ToRadix2(res0, 16, 2);
    a1 = FromRadix1ToRadix2(a1, 16, 2);
    a1 = FromRadix1ToRadix2(AdditionByModule(res1, a1, 2), 2, 16);
    return {
        a1: a0,
        a0: a1
    };
};

// G*
var GG = function (key, a1, a0) {
    var object = G(key, a1, a0);
    return String(object.a0) + String(object.a1);
};

// Развёртывание ключа
// K = ffeeddccbbaa99887766554433221100f0f1f2f3f4f5f6f7f8f9fafbfcfdfeff
var keyDeployment = function (key) {
    var key = String(key);
    var result = [];
    var offset;
    for (var i = 0; i < 32; i++) {
        if (i < 24) {
            offset = (i % 8) * 8;
            result[i] = key.substr(offset, 8);
        } else {
            offset = (8 - i % 8) * 8;
            result[i] = key.substr(offset - 8, 8);
        }
    }
    return result;
};

//#endregion